package edu.ntnu.idatt2001.cardgame;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.*;

public class DeckOfCards {
    private List<PlayingCard> cards;

    /**
     * Creates a new deck of cards with all 52 cards in sorted order.
     */
    public DeckOfCards() {
        cards = new ArrayList<>(52);
        char[] suits = { 'S', 'H', 'D', 'C' };
        for (char suit : suits) {
            for (int face = 1; face <= 13; face++) {
                cards.add(new PlayingCard(suit, face));
            }
        }
    }
    public List<PlayingCard> getDeck() {
        return cards;
    }

    /**
     * Shuffles the deck of cards.
     */
    public void shuffle() {
        Collections.shuffle(cards);
    }

    /**
     * Returns the number of cards in the deck.
     *
     * @return the number of cards in the deck
     */
    public int size() {
        return cards.size();
    }

    /**
     * Deals the specified number of cards from the deck.
     *
     * @param numCards the number of cards to deal
     * @return a list of the dealt cards
     * @throws IllegalArgumentException if numCards is greater than the number of cards in the deck
     */
    public List<PlayingCard> deal(int numCards) {
        if (numCards > cards.size()) {
            throw new IllegalArgumentException("Cannot deal more cards than are in the deck");
        }
        List<PlayingCard> hand = new ArrayList<>(numCards);
        for (int i = 0; i < numCards; i++) {
            hand.add(cards.remove(0));
        }
        return hand;
    }

    /**
     * "Deal a hand of n cards from the deck, removing them from the deck."
     *
     * The function starts by creating a new empty list to hold the hand. It then checks to make sure that there are enough
     * cards in the deck to deal the hand. If there aren't, it throws an exception
     *
     * @param n the number of cards to deal
     * @return A list of PlayingCard objects.
     */
    public List<PlayingCard> dealHand(int n) {
        List<PlayingCard> hand = new ArrayList<>();
        if (n > cards.size()) {
            throw new IllegalArgumentException("Not enough cards in the deck");
        }
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            int index = random.nextInt(cards.size());
            hand.add(cards.remove(index));
        }
        return hand;
    }

    /**
     * It clears the deck and creates a new deck of cards, shuffles it, and then returns it
     */
    public void reset() {
        cards.clear();
        for (char suit : new char[]{'H', 'D', 'C', 'S'}) {
            for (int face = 1; face <= 13; face++) {
                cards.add(new PlayingCard(suit, face));
            }
        }
        shuffle();
    }
}
