package edu.ntnu.idatt2001.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A simple GUI for a card game. Allows the user to deal a hand of 5 cards
 * and check various properties of the hand, such as the sum of the face values,
 * whether the hand contains hearts or the queen of spades, and whether the hand
 * constitutes a flush (5 cards of the same suit).
 */
public class CardGameGUI extends Application {

    private DeckOfCards deck = new DeckOfCards();
    private List<PlayingCard> hand;

    /**
     * Launches the JavaFX application.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Sets up the GUI components and event handlers.
     *
     * @param primaryStage the main window of the application
     * @throws Exception if an error occurs during initialization
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Card Game");

        // Create layout
        GridPane layout = new GridPane();
        layout.setHgap(10);
        layout.setVgap(10);
        layout.setPadding(new Insets(10));

        // Create controls
        Label handLabel = new Label("Your hand:");
        Label handText = new Label();

        Button dealButton = new Button("Deal hand");
        dealButton.setOnAction(event -> {
            try {
                hand = deck.dealHand(5);
                StringBuilder sb = new StringBuilder();
                for (PlayingCard card : hand) {
                    sb.append(card.getAsString()).append(" ");
                }
                handText.setText(sb.toString());
            } catch (IllegalArgumentException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
                alert.showAndWait();
            }
        });


        Button checkButton = new Button("Check hand");
        checkButton.setOnAction(event -> {
            int sum = hand.stream().mapToInt(PlayingCard::getFace).sum();
            boolean hasHearts = hand.stream().anyMatch(card -> card.getSuit() == 'H');
            boolean hasSpadesQueen = hand.stream().anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);

            boolean isFlush = false;
            for (char suit : new char[]{'H', 'D', 'C', 'S'}) {
                long count = hand.stream().filter(card -> card.getSuit() == suit).count();
                if (count == 5) {
                    isFlush = true;
                    break;
                }
            }

            String[][] data = {
                    {"Sum of face values:", Integer.toString(sum)},
                    {"Queen of spades in your hand:", hasSpadesQueen ? "Yes" : "No"},
                    {"Flush in your hand:", isFlush ? "Yes" : "No"},
                    {"Hearts in hand:", ""}
            };

            GridPane textTable = new GridPane();
            textTable.setHgap(10);
            textTable.setVgap(10);
            for (int i = 0; i < data.length; i++) {
                Label label = new Label(data[i][0]);
                label.setMinWidth(200);
                Label value = new Label(data[i][1]);
                if (i == data.length - 1) {
                    // Align the "Hearts in hand" row with the other rows
                    textTable.addRow(i, label);
                    textTable.add(value, 1, i, 1, 2);
                } else {
                    textTable.addRow(i, label, value);
                }
            }

            if (hasHearts) {
                List<String> heartsCards = hand.stream()
                        .filter(card -> card.getSuit() == 'H')
                        .map(PlayingCard::getAsString)
                        .collect(Collectors.toList());
                String heartsCardsString = String.join(" ", heartsCards);
                Label heartsText = new Label(heartsCardsString);

                textTable.add(new Label("Yes"), 1, data.length - 1);
                textTable.add(heartsText, 1, data.length, 1, 2);
            } else {
                textTable.add(new Label("No"), 1, data.length - 1);
            }

            // Clear the layout before adding the new table
            layout.getChildren().removeIf(node -> node instanceof GridPane);
            layout.add(textTable, 1, 1);
        });
        Button restartButton = new Button("Restart game");
        restartButton.setOnAction(event -> {
            hand = null;
            handText.setText("");
            layout.getChildren().removeIf(node -> node instanceof GridPane);
            deck.reset(); // reset the deck to its initial state
        });


        // Add controls to layout
        layout.add(handLabel, 0, 0);
        layout.add(handText, 1, 0);
        layout.add(dealButton, 0, 2);
        layout.add(checkButton, 1, 2);
        layout.add(restartButton, 2, 2);

        // Create scene and show stage
        Scene scene = new Scene(layout, 600, 500);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
