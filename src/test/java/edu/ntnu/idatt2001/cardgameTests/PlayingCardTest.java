package edu.ntnu.idatt2001.cardgameTests;

import edu.ntnu.idatt2001.cardgame.PlayingCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PlayingCardTest {

    @Test
    public void testGetAsString() {
        PlayingCard card = new PlayingCard('H', 10);
        String expected = "H10";
        String actual = card.getAsString();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetSuit() {
        PlayingCard card = new PlayingCard('S', 1);
        char expected = 'S';
        char actual = card.getSuit();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetFace() {
        PlayingCard card = new PlayingCard('D', 13);
        int expected = 13;
        int actual = card.getFace();
        Assertions.assertEquals(expected, actual);
    }
}
