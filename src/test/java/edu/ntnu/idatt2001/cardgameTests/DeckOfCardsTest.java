package edu.ntnu.idatt2001.cardgameTests;

import edu.ntnu.idatt2001.cardgame.DeckOfCards;
import edu.ntnu.idatt2001.cardgame.PlayingCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class DeckOfCardsTest {

    private DeckOfCards deck;

    @BeforeEach
    public void setUp() {
        deck = new DeckOfCards();
    }

    @Test
    public void testSize() {
        int expected = 52;
        int actual = deck.size();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetDeck() {
        List<PlayingCard> actual = deck.getDeck();
        Assertions.assertNotNull(actual);
        Assertions.assertEquals(52, actual.size());
    }

    @Test
    public void testShuffle() {
        List<PlayingCard> original = new ArrayList<>(deck.getDeck());
        deck.shuffle();
        List<PlayingCard> shuffled = deck.getDeck();
        boolean orderChanged = false;
        for (int i = 0; i < original.size(); i++) {
            if (!original.get(i).equals(shuffled.get(i))) {
                orderChanged = true;
                break;
            }
        }
        Assertions.assertTrue(orderChanged);
    }



    @Test
    public void testDeal() {
        int numCards = 5;
        List<PlayingCard> hand = deck.deal(numCards);
        Assertions.assertEquals(numCards, hand.size());
        Assertions.assertEquals(52 - numCards, deck.size());
    }

    @Test
    public void testDealHand() {
        int numHands = 4;
        int cardsPerHand = 5;
        List<List<PlayingCard>> hands = new ArrayList<>();
        for (int i = 0; i < numHands; i++) {
            hands.add(deck.dealHand(cardsPerHand));
        }
        Assertions.assertEquals(numHands * cardsPerHand, hands.stream().flatMap(List::stream).count());
    }
}

